/**
 * @file
 * 
 * @section license License
 * 
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef MAGIC_SQUARE_ESSENTIAL_H
#define MAGIC_SQUARE_ESSENTIAL_H

#include "tab_uint_functions.h"
#include "square_uint_essential.h"
#include "square_uint_sum.h"


/**
 * Create a magic square.
 * @param size Size of the magic square
 * @return square A magic square
 */
square_uint
magic_square_create(unsigned int size);

/**
 * Fill magically a square if his size is an odd number (nb % 2 == 1).
 * @param square A magic square pointer
 */
void
square_uint_odd_fill_magically(square_uint* square);

/**
 * Fill magically a square if his size is a doubly even number (nb % 4 == 0).
 * @param square A magic square pointer
 */
void
square_uint_doubly_even_fill_magically(square_uint* square);

/**
 * Fill magically a square
 * @param square A magic square pointer
 */
void
square_uint_fill_magically(square_uint* square);

/**
 * Returns the magic constant of a magic square with no check.
 * There is no check of null pointers and if the square is magic.
 * @param square A magic square pointer
 * @return The magic constant of a magic square
 */
unsigned int
magic_square_get_magic_constant_unsafe(const square_uint* square);

/**
 * Returns the magic constant of a magic square with checks.
 * @param square A magic square pointer
 * @return The magic constant of a magic square
 */
unsigned int
magic_square_get_magic_constant(const square_uint* square);

/**
 * Returns if a square is magic.
 * @param square A square pointer
 * @return True if a square is magic, otherwise false.
 */
bool
is_magic_square(const square_uint* square);

#endif
