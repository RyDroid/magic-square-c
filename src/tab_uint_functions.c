/**
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "tab_uint_functions.h"


bool
tab_uint_value_is_in(const unsigned int * tab,
		     size_t tab_size,
		     unsigned int value_searched)
{
  if(tab == NULL)
    {
      return false;
    }
  
  for(size_t i=0; i < tab_size; ++i)
    {
      if(tab[i] == value_searched)
	return true;
    }
  return false;
}

bool
tab_uint_compare(const unsigned int * tab1,
		 const unsigned int * tab2,
		 size_t tab_size)
{
  if(tab1 == tab2)
    {
      return true;
    }
  if(tab1 == NULL || tab2 == NULL)
    {
      return false;
    }
  
  for(unsigned int i=0; i < tab_size; ++i)
    {
      if(tab1[i] != tab2[i])
	{
	  return false;
	}
    }
  return true;
}

bool
tab_uint_are_all_values_different(const unsigned int * tab,
				  size_t tab_size)
{
  if(tab == NULL)
    {
      return false;
    }
  
  for(size_t i=1; i < tab_size; ++i)
    {
      const unsigned int * pointer_start_tab = tab + i * sizeof(unsigned int);
      if( tab_uint_value_is_in(pointer_start_tab,  tab_size - i,  *(pointer_start_tab - sizeof(unsigned int))) )
	{
	  return false;
	}
    }
  return true;
}
