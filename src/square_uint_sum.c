/**
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "square_uint_sum.h"


unsigned int
square_uint_line_sum_unsafe(const square_uint* square,
			    unsigned int line)
{
  unsigned int sum = 0;
  for(size_t column=0; column < square->size; ++column)
    {
      sum += square_uint_get_element_value_unsafe(square, line, column);
    }
  return sum;
}

unsigned int
square_uint_line_sum(const square_uint* square,
		     unsigned int line)
{
  return
    square_uint_is_init(square) && square_uint_line_exists(square, line)
    ? square_uint_line_sum_unsafe(square, line) : 0;
}

unsigned int
square_uint_column_sum_unsafe(const square_uint* square,
			      unsigned int column)
{
  unsigned int sum = 0;
  for(size_t line=0; line < square->size; ++line)
    {
      sum += square_uint_get_element_value_unsafe(square, line, column);
    }
  return sum;
}

unsigned int
square_uint_column_sum(const square_uint* square,
				    unsigned int column)
{
  return
    square_uint_is_init(square) && square_uint_column_exists(square, column)
    ? square_uint_column_sum_unsafe(square, column) : 0;
}

unsigned int
square_uint_diagonal_top_left_bottom_right_sum_unsafe(const square_uint* square)
{
  unsigned int sum = 0;
  for(size_t i=0; i < square->size; ++i)
    {
      sum += square_uint_get_element_value_unsafe(square, i, i);
    }
  return sum;
}

unsigned int
square_uint_diagonal_top_left_bottom_right_sum(const square_uint* square)
{
  return
    square_uint_is_init(square)
    ? square_uint_diagonal_top_left_bottom_right_sum_unsafe(square)
    : 0;
}

unsigned int
square_uint_diagonal_top_right_bottom_left_sum_unsafe(const square_uint* square)
{
  unsigned int sum = 0;
  for(size_t i=0; i < square->size; ++i)
    {
      sum += square_uint_get_element_value_unsafe(square, i, square->size - i -1);
    }
  return sum;
}

unsigned int
square_uint_diagonal_top_right_bottom_left_sum(const square_uint* square)
{
  return
    square_uint_is_init(square)
    ? square_uint_diagonal_top_right_bottom_left_sum_unsafe(square)
    : 0;
}
