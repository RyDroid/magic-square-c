/**
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Nicola Spanti (RyDroid)
 */


#include "square_uint_print.h"


void
square_uint_print_without_grid(const square_uint* square)
{
  if(square != NULL && square->tab != NULL)
    {
      size_t line, column;
      for(line=0; line < square->size; ++line)
	{
	  for(column=0; column < square->size; ++column)
	    {
	      printf("\t%u", square_uint_get_element_value_unsafe(square, line, column));
	    }
	  puts("");
	}
    }
}

void
square_uint_print_with_ascii_grid(const square_uint* square)
{
  if(square != NULL && square->tab != NULL)
    {
      size_t line, column;
      for(line=0; line < square->size; ++line)
	{
	  for(column=0; column < square->size; ++column)
	    {
	      printf("|%u\t", square_uint_get_element_value_unsafe(square, line, column));
	    }
	  puts(" |");
	}
    }
}

void
square_uint_print_with_cornered_grid(const square_uint* square)
{
  if(square != NULL && square->tab != NULL)
    {
      size_t line, column;
      for(line=0; line < square->size; ++line)
	{
	  for(column=0; column < square->size; ++column)
	    {
	      if(line == 0)
		{
		  if(column == 0)
		    printf("┏");
		  else
		    printf("┳");
		}
	      else if(line == square->size -1)
		{
		  if(column == 0)
		    printf("┗");
		  else
		    printf("┻");
		}
	      else
		{
		  if(column == 0)
		    printf("┣");
		  else
		    printf("╋");
		}
	      printf("\t%u\t", square_uint_get_element_value_unsafe(square, line, column));
	    }
	  
	  if(line == 0)
	    puts("┓");
	  else if(line == square->size -1)
	    puts("┛");
	  else
	    puts("┫");
	}
    }
}
