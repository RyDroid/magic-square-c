/**
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Nicola Spanti (RyDroid)
 */


#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "magic_square_essential.h"
#include "square_uint_io.h"


int main(int argc, char* argv[])
{
  if(argc > 1)
    {
      if(strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "--aide") == 0)
	{
	  puts(argv[0]); /* TODO */
	  return EXIT_SUCCESS;
	}
      if(strcmp(argv[1], "--license") == 0 || strcmp(argv[1], "--licence") == 0)
	{
	  puts("This program is under GNU Lesser General Public License 3 as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.");
	  return EXIT_SUCCESS;
	}
      if(strcmp(argv[1], "--author") == 0 || strcmp(argv[1], "--auteur") == 0)
	{
	  puts("The author of this program is Nicola Spanti, also known as RyDroid.");
	  return EXIT_SUCCESS;
	}
      
      /* TODO
	 create with strings (parse)
	 0  1  2  3  4  5  6  7  8
	 or
	 0, 1, 2; 3, 4, 5; 6, 7, 8
	 or
	 { {0, 1, 2}, {3, 4, 5}, {6, 7, 8} }
	 or
	 0  1  2
	 3  4  5
	 6  7  8
	 or
	 mix of these strings
       */
    }
 

  unsigned int number_of_lines;
  printf("Enter a number : ");
  scanf("%u", &number_of_lines);
  
  square_uint square = magic_square_create(number_of_lines);
  square_uint_print_without_grid(&square);
  square_uint_print_with_ascii_grid(&square);
  square_uint_print_with_cornered_grid(&square);
  
  if(is_magic_square(&square))
    {
      puts("It is a magic square. :)");
      printf("The magic constant is %u.\n", magic_square_get_magic_constant(&square));
    }
  else
    {
      puts("It is not a magic square. :(");
    }
  
  square_uint_scan(&square);
  square_uint_print_without_grid(&square);
  
  if(is_magic_square(&square))
    {
      puts("It is a magic square. :)");
      printf("The magic constant is %u.\n", magic_square_get_magic_constant(&square));
    }
  else
    {
      puts("It is not a magic square. :(");
    }
  
  square_uint_destruct(&square);
  
  
  return EXIT_SUCCESS;
}
