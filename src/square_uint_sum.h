/**
 * @file
 * 
 * @section license License
 * 
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef SQUARE_UINT_SUM_H
#define SQUARE_UINT_SUM_H

#include "square_uint_essential.h"


/**
 * Returns the sum of a line of a square of unsigned int with no check.
 * @param square A square of unsigned int pointer
 * @param line A line of the square of unsigned int
 * @return The sum of a line of a square of unsigned int
 */
unsigned int
square_uint_line_sum_unsafe(const square_uint* square,
			    unsigned int line);

/**
 * Returns the sum of a line of a square of unsigned int with checks.
 * @param square A square of unsigned int pointer
 * @param line A line of the square of unsigned int
 * @return The sum of a line of a square of unsigned int
 */
unsigned int
square_uint_line_sum(const square_uint* square,
		     unsigned int line);

/**
 * Returns the sum of a column of a square of unsigned int with no check.
 * @param square A square of unsigned int pointer
 * @param column A column of the square of unsigned int
 * @return The sum of a column of a square of unsigned int.
 */
unsigned int
square_uint_column_sum_unsafe(const square_uint* square,
			      unsigned int column);

/**
 * Returns the sum of a column of a square of unsigned int with no check.
 * @param square A square of unsigned int pointer
 * @param column A column of the square of unsigned int
 * @return The sum of a column of a square of unsigned int.
 */
unsigned int
square_uint_column_sum(const square_uint* square,
		       unsigned int column);

/**
 * Returns the sum of the diagonal that starts at the top left and finishes at the bottom right with no check.
 * @param square A square of unsigned int pointer
 * @return The sum of the diagonal that starts at the top left and finishes at the bottom right.
 */
unsigned int
square_uint_diagonal_top_left_bottom_right_sum_unsafe(const square_uint* square);

/**
 * Returns the sum of the diagonal that starts at the top left and finishes at the bottom right with checks.
 * @param square A square of unsigned int pointer
 * @return The sum of the diagonal that starts at the top left and finishes at the bottom right.
 */
unsigned int
square_uint_diagonal_top_left_bottom_right_sum(const square_uint* square);

/**
 * Returns the sum of the diagonal that starts at the top right and finishes at the bottom left with no check.
 * @param square A square of unsigned int pointer
 * @return The sum of the diagonal that starts at the top right and finishes at the bottom left.
 */
unsigned int
square_uint_diagonal_top_right_bottom_left_sum_unsafe(const square_uint* square);

/**
 * Returns the sum of the diagonal that starts at the top right and finishes at the bottom left with checks.
 * @param square A square of unsigned int pointer
 * @return The sum of the diagonal that starts at the top right and finishes at the bottom left.
 */
unsigned int
square_uint_diagonal_top_right_bottom_left_sum(const square_uint* square);

#endif

