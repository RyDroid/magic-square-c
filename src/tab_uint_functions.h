/**
 * @file
 * 
 * @section license License
 * 
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef TAB_UINT_FUNCTIONS_H
#define TAB_UINT_FUNCTIONS_H

#include <stdlib.h>
#include "bool.h"


/**
 * Returns true if a value is in a tab of unsigned int, otherwise false.
 * @param tab A tab of unsigned int
 * @param tab_size The size of the tab
 * @param value_searched The value searched
 * @return True if a value is in a tab of unsigned int, otherwise false.
 */
bool
tab_uint_value_is_in(const unsigned int * tab,
		     size_t tab_size,
		     unsigned int value_searched);

/**
 * Returns true if all values tab1[line][column] equals tab2[line][column].
 * @param tab1 A first tab of unsigned int
 * @param tab2 A second tab of unsigned int
 * @param tab_size The size of the tabs
 * @return True if all values  tab1[line][column] equals tab2[line][column], otherwise false.
 */
bool
tab_uint_compare(const unsigned int * tab1,
		 const unsigned int * tab2,
		 size_t tab_size);

/**
 * Returns true if all values of a tab are different.
 * @param tab A tab of unsigned int
 * @param tab_size The size of the tab
 * @return True if all values of a tab are different, otherwise false.
 */
bool
tab_uint_are_all_values_different(const unsigned int * tab,
				  size_t tab_size);

#endif
