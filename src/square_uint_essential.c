/**
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "square_uint_essential.h"


void
square_uint_init_unsafe(square_uint* square,
			unsigned int size)
{
  square->tab = (unsigned int *) malloc(sizeof(unsigned int) * size * size);
  square->size = size;
}

void
square_uint_init_one_time_only(square_uint* square,
			       unsigned int size)
{
  if(square != NULL)
    {
      square_uint_init_unsafe(square, size);
    }
}

void
square_uint_init(square_uint* square,
		 unsigned int size)
{
  if(!square_uint_is_init(square))
    {
      square_uint_init_unsafe(square, size);
    }
}

square_uint
square_uint_create(unsigned int size)
{
  square_uint square = {NULL, 0};
  square_uint_init_unsafe(&square, size);
  return square;
}

void square_uint_destruct(square_uint* square)
{
  if(square != NULL)
    {
      if(square->tab != NULL)
	{
	  free(square->tab);
	  square->tab = NULL;
	}
      square->size = 0;
    }
}

bool
square_uint_is_init(const square_uint* square)
{
  return
    square != NULL &&
    square->tab != NULL &&
    square->size > 0;
}

bool
square_uint_line_exists(const square_uint* square,
			unsigned int line)
{
  return
    square_uint_is_init(square) &&
    line < square->size;
}

bool
square_uint_column_exists(const square_uint* square,
			  unsigned int column)
{
  return
    square_uint_is_init(square) &&
    column < square->size;
}

bool
square_uint_element_exists(const square_uint* square,
			   unsigned int line,
			   unsigned int column)
{
  return
    square_uint_is_init(square) &&
    line < square->size &&
    column < square->size;
}

unsigned int*
square_uint_get_element_pointer_unsafe(const square_uint* square,
				       unsigned int line,
				       unsigned int column)
{
  return square->tab + sizeof(unsigned int) * (line * square->size + column);
}

unsigned int*
square_uint_get_element_pointer(const square_uint* square,
				unsigned int line,
				unsigned int column)
{
  return
    square_uint_element_exists(square, line, column)
    ? square_uint_get_element_pointer_unsafe(square, line, column)
    : NULL;
}

unsigned int
square_uint_get_element_value_unsafe(const square_uint* square,
				     unsigned int line,
				     unsigned int column)
{
  return *square_uint_get_element_pointer_unsafe(square, line, column);
}

unsigned int
square_uint_get_element_value(const square_uint* square,
			      unsigned int line,
			      unsigned int column)
{
  unsigned int * pointer = square_uint_get_element_pointer(square, line, column);
  return pointer == NULL ? 0 : *pointer;
}

void
square_uint_set_element_value_unsafe(square_uint* square,
				     unsigned int line,
				     unsigned int column,
				     unsigned int value)
{
  *square_uint_get_element_pointer_unsafe(square, line, column) = value;
}

void
square_uint_set_element_value(square_uint* square,
			      unsigned int line,
			      unsigned int column,
			      unsigned int value)
{
  unsigned int * pointer = square_uint_get_element_pointer(square, line, column);
  if(pointer != NULL)
    {
      *pointer = value;
    }
}

void
square_uint_fill_with_const(square_uint* square,
			    unsigned int value_for_filling)
{
  if(square_uint_is_init(square))
    {
      unsigned int line, column;
      for(line=0; line < square->size; ++line)
	{
	  for(column=0; column < square->size; ++column)
	    {
	      square_uint_set_element_value_unsafe(square, line, column, value_for_filling);
	    }
	}
    }
}
