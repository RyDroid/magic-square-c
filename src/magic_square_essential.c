/**
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "magic_square_essential.h"


square_uint
magic_square_create(unsigned int size)
{
  square_uint square = square_uint_create(size);
  square_uint_fill_magically(&square);
  return square;
}

void
square_uint_odd_fill_magically(square_uint* square)
{
  if(square_uint_is_init(square) && square->size % 2 == 1)
    {
      square_uint_fill_with_const(square, 0);
      
      unsigned int column = square->size / 2;
      unsigned int line = column + 1;
  
      for(unsigned int i=1, size2 = square->size * square->size;
	  i <= size2;
	  ++i)
	{
	  if(square_uint_get_element_value_unsafe(square, line, column) != 0)
	    {
	      line = (line +1) % square->size;
	      column = (column -1 + square->size) % square->size;
	    }
      
	  square_uint_set_element_value_unsafe(square, line, column, i);
	  line = (line +1) % square->size;
	  column = (column +1) % square->size;
	}
    }
}

void
square_uint_doubly_even_fill_magically(square_uint* square)
{
  /* It is a modified version of a method of Paul Leahy
   * http://java.about.com/od/beginnerlevel/a/doublyevenmagic.htm
   * Unfortunately, there is no information about the license, so it might be a legal problem
   */
  
  if(square_uint_is_init(square) && square->size % 4 == 0)
    {
      square_uint_fill_with_const(square, 0);
      
      unsigned int count_number = 1;
      unsigned int mini_square_number = square->size / 4;
      unsigned int line, column;
      unsigned int used_number[square->size * square->size /2];
      unsigned int used_number_counter = 0;
  
      for (line=0; line < square->size; ++line)
	{
	  // populate the corner mini-squares
	  if (line < mini_square_number || line > square->size - mini_square_number -1)
	    {
	      for (column=0; column < mini_square_number; ++column)
		{
		  square_uint_set_element_value_unsafe(square, line, column, count_number);
		  used_number[used_number_counter++] = count_number;
		  ++count_number;
		}
	      count_number += mini_square_number + mini_square_number;
	      for (column=square->size-mini_square_number; column < square->size; ++column)
		{
		  square_uint_set_element_value_unsafe(square, line, column, count_number);
		  used_number[used_number_counter++] = count_number;
		  ++count_number;
		}
	    }
	  else // populate the large center square
	    {
	      count_number += mini_square_number;
	      for (column=mini_square_number; column < square->size - mini_square_number; ++column)
		{
		  square_uint_set_element_value_unsafe(square, line, column, count_number);
		  used_number[used_number_counter++] = count_number;
		  ++count_number;
		}
	      count_number += mini_square_number;
	    }
	}
  
      count_number = square->size * square->size - mini_square_number;
      // populate remaining squares
      for (line=0; line < square->size; ++line)
	{
	  for (column=0; column < square->size; ++column)
	    {
	      if (square_uint_get_element_value_unsafe(square, line, column) == 0)
		{
		  while(tab_uint_value_is_in(used_number, used_number_counter, count_number))
		    --count_number;
           
		  square_uint_set_element_value_unsafe(square, line, column, count_number--);
		}
	    }
	}
    }
}

void
square_uint_fill_magically(square_uint* square)
{
  if(square->size % 2 == 1)
    square_uint_odd_fill_magically(square);
  else if(square->size % 4 == 0)
    square_uint_doubly_even_fill_magically(square);
}

unsigned int
magic_square_get_magic_constant_unsafe(const square_uint* square)
{
  return square_uint_diagonal_top_left_bottom_right_sum_unsafe(square);
}

unsigned int
magic_square_get_magic_constant(const square_uint* square)
{
  return square_uint_diagonal_top_left_bottom_right_sum(square);
}

bool is_magic_square(const square_uint* square)
{
  if(!square_uint_is_init(square) ||
     !tab_uint_are_all_values_different(square->tab, square->size * square->size))
    {
      return false;
    }
  
  unsigned int MAGIC_CONST = square_uint_diagonal_top_left_bottom_right_sum_unsafe(square);
  
  if(MAGIC_CONST != square_uint_diagonal_top_right_bottom_left_sum_unsafe(square))
    {
      return false;
    }
  
  for(size_t i=0; i < square->size; ++i)
    {
      if(MAGIC_CONST != square_uint_line_sum_unsafe(square, i) ||
	 MAGIC_CONST != square_uint_column_sum_unsafe(square, i))
	{
	  return false;
	}
    }
  
  return true;
}
