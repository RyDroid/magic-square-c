/**
 * @file
 * 
 * @section license License
 * 
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef SQUARE_UINT_ESSENTIAL_H
#define SQUARE_UINT_ESSENTIAL_H

#include <stdlib.h>
#include "bool.h"


typedef struct square_uint square_uint;

/**
 * A struct that defines a square of unsigned int.
 * It has a tab and the size of line and column.
 */
struct square_uint
{
  /**
   * The content of the square of unsigned int.
   * Only functions should use it.
   */
  unsigned int* tab;
  
  /**
   * The number of lines and columns.
   * size * size is the size of the tab.
   */
  unsigned int size;
};


/**
 * Initialize the square of unsigned int unsafely.
 * There is no check on pointers.
 * @param square A square of unsigned int pointer
 * @param size Size of the square of unsigned int
 */
void
square_uint_init_unsafe(square_uint* square,
			unsigned int size);

/**
 * Initialise the square of unsigned int for the first time.
 * tab should be NULL.
 * @param square A square of unsigned int pointer
 * @param size Size of the square of unsigned int
 */
void
square_uint_init_one_time_only(square_uint* square,
			       unsigned int size);

/**
 * Initialize the square of unsigned int safely.
 * There are checks on pointer.
 * @param square A square of unsigned int pointer
 * @param size Size of the square of unsigned int
 */
void
square_uint_init(square_uint* square,
		 unsigned int size);

/**
 * Create a square of unsigned int.
 * @param size Size of the square of unsigned int
 * @return square A square of unsigned int
 */
square_uint
square_uint_create(unsigned int size);

/**
 * Destructs properly a square of unsigned int.
 * @param square A square of unsigned int pointer
 */
void
square_uint_destruct(square_uint* square);

/**
 * Returns true if the square of unsigned int is init.
 * @param square A square of unsigned int pointer
 * @return True if the square of unsigned int is init, otherwise false.
 */
bool
square_uint_is_init(const square_uint* square);

/**
 * Returns true if a line of the square of unsigned int is init.
 * @param square A square of unsigned int pointer
 * @param line A line of the square of unsigned int
 * @return True if a line of the square of unsigned int exists, otherwise false
 */
bool
square_uint_line_exists(const square_uint* square,
			unsigned int line);

/**
 * Returns true if a column of the square of unsigned int is init.
 * @param square A square of unsigned int pointer
 * @param column A column of the square of unsigned int
 * @return True if a column of the square of unsigned int exists, otherwise false
 */
bool
square_uint_column_exists(const square_uint* square,
			  unsigned int column);

/**
 * Returns true if an element of the square of unsigned int is init.
 * @param square A square of unsigned int pointer
 * @param line A line of the square of unsigned int
 * @param column A column of the square of unsigned int
 * @return True if an element of the square of unsigned int exists, otherwise false.
 */
bool
square_uint_element_exists(const square_uint* square,
			   unsigned int line,
			   unsigned int column);

/**
 * Returns the pointer of an element of a square of unsigned int with no check.
 * There is no check of the existence of the square, the tab of the square, the line and the column.
 * It is useful for optimization, but seg fault and null pointer can occur.
 * @param square A square of unsigned int pointer
 * @param line A line of the square of unsigned int
 * @param column A column of the square of unsigned int
 * @return The pointer of the element (line, column) of the square
 */
unsigned int*
square_uint_get_element_pointer_unsafe(const square_uint* square,
				       unsigned int line,
				       unsigned int column);

/**
 * Returns the pointer of an element of a square of unsigned int with checks.
 * @param square A square of unsigned int pointer
 * @param line A line of the square of unsigned int
 * @param column A column of the square of unsigned int
 * @return The pointer of the element (line, column) of the square
 */
unsigned int*
square_uint_get_element_pointer(const square_uint* square,
				unsigned int line,
				unsigned int column);

/**
 * Returns the value of an element of a square of unsigned int with no check.
 * @param square A square of unsigned int pointer
 * @param line A line of the square of unsigned int
 * @param column A column of the square of unsigned int
 * @return The value of the element (line, column) of the square
 */
unsigned int
square_uint_get_element_value_unsafe(const square_uint* square,
				     unsigned int line,
				     unsigned int column);

/**
 * Returns the value of an element of a square of unsigned int with checks.
 * @param square A square of unsigned int pointer
 * @param line A line of the square of unsigned int
 * @param column A column of the square of unsigned int
 * @return The value of the element (line, column) of the square
 */
unsigned int
square_uint_get_element_value(const square_uint* square,
			      unsigned int line,
			      unsigned int column);

/**
 * Set the value of the element (line, column) of a square of unsigned int with no check.
 * @param square A square of unsigned int pointer
 * @param line A line of the square of unsigned int
 * @param column A column of the square of unsigned int
 * @param value New value of the element
 */
void
square_uint_set_element_value_unsafe(square_uint* square,
				     unsigned int line,
				     unsigned int column,
				     unsigned int value);

/**
 * Set the value of the element (line, column) of a square of unsigned int with checks.
 * @param square A square of unsigned int pointer
 * @param line A line of the square of unsigned int
 * @param column A column of the square of unsigned int
 * @param value New value of the element
 */
void
square_uint_set_element_value(square_uint* square,
			      unsigned int line,
			      unsigned int column,
			      unsigned int value);

/**
 * Fill a square of unsigned int with a constant.
 * @param square A square of unsigned int pointer
 * @param value_for_filling A value for filling
 */
void square_uint_fill_with_const(square_uint* square,
				 unsigned int value_for_filling);

/* TODO
unsigned int * square_uint_get_line_copy(square* square, unsigned int line);
unsigned int * square_uint_get_column_copy(square* square, unsigned int column);
 */

#endif
