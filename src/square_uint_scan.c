/**
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Nicola Spanti (RyDroid)
 */


#include "square_uint_scan.h"


void
square_uint_scan(square_uint* square)
{
  if(square_uint_is_init(square))
    {
      for(unsigned int line=0, column; line < square->size; ++line)
	{
	  for(column=0; column < square->size; ++column)
	    {
	      printf("square[line=%u][column=%u] = ", line, column);
	      scanf("%u", square_uint_get_element_pointer(square, line, column));
	    }
	}
    }
}
