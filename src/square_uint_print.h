/**
 * @file
 * 
 * @section license License
 * 
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef SQUARE_UINT_PRINT_H
#define SQUARE_UINT_PRINT_H

#include <stdio.h>
#include "square_uint_essential.h"


/**
 * Print a square of unsigned int not in a grid to stdout.
 * @param square A square of unsigned int pointer
 */
void
square_uint_print_without_grid(const square_uint* square);

/**
 * Print a square of unsigned int in an ASCII grid to stdout.
 * @param square A square of unsigned int pointer
 */
void
square_uint_print_with_ascii_grid(const square_uint* square);

/**
 * Print a square of unsigned int in a cornered grid to stdout.
 * @param square A square of unsigned int pointer
 */
void
square_uint_print_with_cornered_grid(const square_uint* square);

#endif
