# Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


SRC_DIR=src
BIN_DIR=bin
DOC_DIR=doc

CC=gcc
INCLUDES=-I./$(SRC_DIR)
DEBUG_FLAGS= #-g
CFLAGS=-std=c99 -Wall -Wextra -Wpedantic $(INCLUDES) $(DEBUG_FLAGS)

PACKAGE=magic-square
FILES_TO_ARCHIVE=$(SRC_DIR)/ makefile etc/ LICENSE* mainpage.dox


.PHONY: $(DOC_DIR)

all: bin doc default-archive


bin: $(BIN_DIR)/main-c

$(BIN_DIR)/main-c: \
		$(BIN_DIR)/tab_uint_functions.o \
		$(BIN_DIR)/square_uint_essential.o \
		$(BIN_DIR)/square_uint_sum.o \
		$(BIN_DIR)/square_uint_print.o \
		$(BIN_DIR)/square_uint_scan.o \
		$(BIN_DIR)/magic_square_essential.o \
		$(BIN_DIR)/main-c.o
	$(CC) $(CFLAGS) \
		$(BIN_DIR)/tab_uint_functions.o \
		$(BIN_DIR)/square_uint_essential.o \
		$(BIN_DIR)/square_uint_sum.o \
		$(BIN_DIR)/square_uint_print.o \
		$(BIN_DIR)/square_uint_scan.o \
		$(BIN_DIR)/magic_square_essential.o \
		$(BIN_DIR)/main-c.o \
		-o $(BIN_DIR)/main-c

$(BIN_DIR)/main-c.o: $(SRC_DIR)/main-c.c $(SRC_DIR)/square_uint_print.h
	$(CC) $(CFLAGS) -c $(SRC_DIR)/main-c.c -o $(BIN_DIR)/main-c.o

$(BIN_DIR)/magic_square_essential.o: $(SRC_DIR)/tab_uint_functions.h $(SRC_DIR)/square_uint_essential.h $(SRC_DIR)/magic_square_essential.h $(SRC_DIR)/magic_square_essential.c
	$(CC) $(CFLAGS) -c $(SRC_DIR)/magic_square_essential.c -o $(BIN_DIR)/magic_square_essential.o

$(BIN_DIR)/square_uint_scan.o: $(SRC_DIR)/square_uint_essential.h $(SRC_DIR)/square_uint_scan.h $(SRC_DIR)/square_uint_scan.c
	$(CC) $(CFLAGS) -c $(SRC_DIR)/square_uint_scan.c -o $(BIN_DIR)/square_uint_scan.o

$(BIN_DIR)/square_uint_print.o: $(SRC_DIR)/square_uint_essential.h $(SRC_DIR)/square_uint_print.h $(SRC_DIR)/square_uint_print.c
	$(CC) $(CFLAGS) -c $(SRC_DIR)/square_uint_print.c -o $(BIN_DIR)/square_uint_print.o

$(BIN_DIR)/square_uint_sum.o: $(SRC_DIR)/square_uint_essential.h $(SRC_DIR)/square_uint_sum.h $(SRC_DIR)/square_uint_sum.c
	$(CC) $(CFLAGS) -c $(SRC_DIR)/square_uint_sum.c -o $(BIN_DIR)/square_uint_sum.o

$(BIN_DIR)/square_uint_essential.o: $(SRC_DIR)/bool.h $(SRC_DIR)/square_uint_essential.h $(SRC_DIR)/square_uint_essential.c
	$(CC) $(CFLAGS) -c $(SRC_DIR)/square_uint_essential.c -o $(BIN_DIR)/square_uint_essential.o

$(BIN_DIR)/tab_uint_functions.o: $(SRC_DIR)/bool.h $(SRC_DIR)/tab_uint_functions.h $(SRC_DIR)/tab_uint_functions.c
	$(CC) $(CFLAGS) -c $(SRC_DIR)/tab_uint_functions.c -o $(BIN_DIR)/tab_uint_functions.o


$(DOC_DIR): $(SRC_DIR)/*.h etc/doxygen_configuration.ini
	mkdir -p $(DOC_DIR)
	doxygen etc/doxygen_configuration.ini
	# PDF with LaTeX
	cd $(DOC_DIR)/latex/ && make


archives: archives-tar zip

archives-tar: tar-gz tar-bz2 tar-xz

default-archive: tar-xz

zip: $(PACKAGE).zip

$(PACKAGE).zip: $(FILES_TO_ARCHIVE)
	zip $(PACKAGE).zip -r -- $(FILES_TO_ARCHIVE)

tar-gz: $(PACKAGE).tar.gz

$(PACKAGE).tar.gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(PACKAGE).tar.gz -- $(FILES_TO_ARCHIVE)

tar-bz2: $(PACKAGE).tar.bz2

$(PACKAGE).tar.bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(PACKAGE).tar.bz2 -- $(FILES_TO_ARCHIVE)

tar-xz: $(PACKAGE).tar.xz

$(PACKAGE).tar.xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(PACKAGE).tar.xz -- $(FILES_TO_ARCHIVE)


clean: clean-bin clean-bin clean-profiling clean-tmp clean-doc clean-archives

clean-bin:
	$(RM) -rf -- \
		$(OBJ_DIR) $(BIN_DIR) \
		*.o *.a *.so *.ko *.lo *.dll *.out
clean-profiling:
	$(RM) -rf -- callgrind.out.*

clean-tmp:
	$(RM) -r -- \
		*~ *.bak *.backup .\#* \#* *.sav *.save *.autosav *.autosave \
		$(SRC_DIR)/*~ $(SRC_DIR)/*.bak $(SRC_DIR)/*.backup $(SRC_DIR)/.\#* $(SRC_DIR)/\#* \
		*.log *.log.* error_log* \
		.cache/ .thumbnails/

clean-doc:
	$(RM) -r -- doc documentation

clean-archives:
	$(RM) -- \
		*.zip *.tar.* *.tgz *.7z *.gz *.bz2 *.lz *.lzma *.xz \
		*.deb *.rpm *.exe *.msi *.dmg *.jar *.apk *.ipa *.iso
